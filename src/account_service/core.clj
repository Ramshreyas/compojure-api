(ns account-service.core
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [schema.core :as s]
            [ring.swagger.schema :as rs]
            [account-service.dummy :refer :all]))

(s/defschema Account
  {:id Long
   :balance s/Num})

(s/defschema NewAccount (dissoc Account :id))

(s/defschema Transfer
  {:id Long
   :from-account s/Int
   :to-account s/Int
   :amount s/Num
   :status s/Keyword})

(s/defschema NewTransfer (dissoc Transfer :id :status))

(def app (api {:swagger {:ui "/"
                         :spec "/swagger.json"
                         :data {:info {:title "Account Service"}
                                :tags [{:name "api"}]}}}
              (context "/api" []
                       :tags ["api"]
                       (POST "/account" []
                            :body [account (describe NewAccount "new account")]
                            :summary "Creates a new account"
                            (ok (add-account account)))

                       (POST "/transfer" []
                             :body [transfer (describe Transfer "new transfer")]
                             :summary "Transfers between accounts"
                             (ok (make-transfer transfer)))
                      
                      (GET "/accounts" []
                          :return [Account]
                          :summary "Gets all accounts"
                          (ok (get-accounts))))))
