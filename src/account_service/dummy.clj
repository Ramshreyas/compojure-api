(ns account-service.dummy)

(defn add-account [account]
    {:id 1
     :balance (:balance account)})

(defn get-account [id]
  {:id id
   :balance 20})

(defn get-accounts []
  [{:id 1
    :balance 10}
    {:id 2
    ;:balance 20
    }])

(defn make-transfer [transfer]
  (let [id (:id transfer)
        from-account (:from-account transfer)
        to-account (:to-account transfer)
        amount (:amount transfer)
        status (:status transfer)]
        {:status "Complete"}))